import Vue from 'vue'
import Router from 'vue-router'
import EjemplosVue from './views/EjemplosVue.vue'
import EjemplosVuex from './views/EjemplosVuex.vue'
import EjemplosBulma from './views/EjemplosBulma.vue'
import Workshop from './views/Workshop.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ejemplos',
      component: EjemplosVue
    },
    {
      path: '/vue',
      name: 'vue',
      component: EjemplosVue
    },
    {
      path: '/vuex',
      name: 'vuex',
      component: EjemplosVuex
    },
    {
      path: '/bulma',
      name: 'bulma',
      component: EjemplosBulma
    },
    {
      path: '/workshop',
      name: 'workshop',
      component: Workshop
    }
  ]
})
