import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    asistente: 'Juan Pablo',
    asistentes: [ 'Juan', 'Marta' ]
  },
  getters: {
    obtenerElUltimoAsistente (state) {
      return state.asistentes[state.asistentes.length -1]
    }
  },
  mutations: {
    agregar (state, asistente) {
      state.asistentes.push(asistente)
    }
  },
  actions: {
    agregarAsistente(context, nombre) {
      context.commit('agregar', nombre)
    }
  }
})
